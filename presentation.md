Corporate reporting in R
========================================================
author: Eduard Parsadanyan
date: 13SEP2018
autosize: true

<!-- Custom CSS -->
<style>
.reveal pre code {
  font-size: 1em;
}
</style>

Let's define corporate reporting
========================================================
- __Multidisciplinary team__<br />can't force everyone to learn Markdown / LaTeX
- __Workflow / pipeline__<br />need to maintain editable documents
- __SOPs or contractual agreements__<br />cant' change the process quickly

Packages written by David Gohel
========================================================

**ReporteRs**

- Will be maintained but will not evolve anymore
- ReporteRs needs rJava with a java version >= 1.6 and <= 1.8
- Does not require any Microsoft component to be used
- It runs on Windows, Linux, Unix and Mac OS systems

***

**Officer**

(+ flextable, rvg, mschart)

>This package is close to ReporteRs as it produces Word and PowerPoint files but it is faster, do not require rJava and is easier to maintain

Today's agenda
========================================================

>We will go through the .docx files creation only

- Create a report from a template file
- Add basic data: text, tables and charts
- Work with Word elements: TOC, titles, sections, page header/footer
- Other tips & tricks

Example 1: Titles & Paragraphs
========================================================




```r
doc <- read_docx("_blank_template.docx")
doc <- doc %>% 
  body_add_toc(pos = "on") %>%
  body_add_break() %>% 
  body_add_par("Section 1. Paragraphs", "heading 1") %>%
  body_add_par(paste(rep("Lorem Ipsum",30),collapse = " "))

  for (i in (1:5)) {
    doc <- doc %>% body_add_par("Lorem Ipsum", style = "List Paragraph") }

  doc <- doc %>% body_add_par(paste(rep("Lorem Ipsum",30),collapse = " "))
  
  for (i in (1:5)) {
    doc <- doc %>% body_add_par("Lorem Ipsum", style = "NumberList") }
  
  print(doc,target = "example 1.docx")
```

Document properties
========================================================


```r
styles_info(doc)

docx_summary(doc)

doc_properties(doc)

docx_dim(doc)
```

Document properties contd.
========================================================


```r
styles_info(doc)
```


```
# A tibble: 14 x 5
   style_type style_id             style_name         is_custom is_default
   <chr>      <chr>                <chr>              <lgl>     <lgl>     
 1 paragraph  Normal               Normal             FALSE     TRUE      
 2 paragraph  Heading1             heading 1          FALSE     FALSE     
 3 paragraph  Heading2             heading 2          FALSE     FALSE     
 4 character  DefaultParagraphFont Default Paragraph~ FALSE     TRUE      
 5 table      TableNormal          Normal Table       FALSE     TRUE      
 6 numbering  NoList               No List            FALSE     TRUE      
 7 character  Heading1Char         Heading 1 Char     TRUE      FALSE     
 8 character  Heading2Char         Heading 2 Char     TRUE      FALSE     
 9 paragraph  Header               header             FALSE     FALSE     
10 character  HeaderChar           Header Char        TRUE      FALSE     
11 paragraph  Footer               footer             FALSE     FALSE     
12 character  FooterChar           Footer Char        TRUE      FALSE     
13 paragraph  ListParagraph        List Paragraph     FALSE     FALSE     
14 paragraph  NumberList           NumberList         TRUE      FALSE     
```

Example 2: Tables
========================================================

```r
df_ <- (head(mtcars)) %>%
  rownames_to_column(var = "CarName")

colrow <- grep("Hornet", df_$CarName)

ft <- df_ %>% 
  flextable() %>% 
  theme_vanilla() %>%
  color(colrow, color = "red") %>% 
  autofit()

doc <- doc %>%
  body_add_break() %>%
  body_add_par("Section 2. Tables", "heading 1") %>%
  body_add_par("Section 2.1. Naive table", "heading 2") %>% 
  body_add_flextable(ft)

print(doc,target = "example 2.docx")
```

Example 2: FlexTables
========================================================
![FlexTables structure](flextable1.png)

Example 2: FlexTables contd.
========================================================
![Default FlexTables structure](flextable2.png)


Example 3: Charts: ggplot vs native MS charts
========================================================

```r
gg <- ggplot(mtcars, aes(x = mpg , y = wt, colour = qsec)) +
  geom_point() + theme_minimal()
ggsave("mtcars.png", gg, width = 6, height = 3)


my_barchart <- ms_barchart(data = browser_data,
               x = "browser", y = "value", group = "serie")
my_barchart <- chart_settings( x = my_barchart,
               dir="vertical", grouping="clustered", gap_width = 50 )
```


Example 3: Printing charts
========================================================

```r
doc <- doc %>%
  body_add_break() %>%
  body_add_par("Section 3. Plots", "heading 1") %>%
  body_add_par("Section 3.1. ggplot", "heading 2") %>% 
  body_add_img(src = "mtcars.png", width = 6, height = 3) %>% 
  body_add_par("Section 3.2. native MS plots", "heading 2") %>% 
  body_add_chart(chart = my_barchart, width = 6, height = 3)
  
print(doc,target = "example 3.docx")
```

Other tricks: batch replace
========================================================

```r
doc <- doc %>%
  headers_replace_all_text("project_name" ,"PROJECT 123") %>% 
  headers_replace_all_text("creation_date",as.character(Sys.Date()))

print(doc,target = "example 4.docx")
```

Other tricks contd.
========================================================
- **body_add vs slip_in**<br />Add element vs insert into an element
- **slip_in_seqfield**<br />Get automated numbering
- **Bookmarks**<br />Add, search, replace
- **Sections**<br />Not only pagebreaks, layout orientation
- **cursor**<br />Manipulate the position of a virtual cursor
- **Office scraping**<br />Extract content from Word

Links
========================================================
<br /><br />
davidgohel.github.io/officer/<br />
davidgohel.github.io/flextable<br />
bitbucket.org/statsconsult/2018-07-officer

Thank you!
========================================================
<br /><br /><br />
__Eduard Parsadanyan__<br />
_Statistical programming & Biostatistics_<br /><br /><br />
eduard@stats-consult.com<br />
+49-176-2800-5275

<!-- Jquery workaround to add slide number: -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

for(i=0;i<$("section").length;i++) {
n=i+1
if(i==0) continue
$("section").eq(i).append("<p style='font-size:20px;position:fixed;right:10px;bottom:10px;'>" + n + " / "+ $("section").length + "</p>")
}

</script>
